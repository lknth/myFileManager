#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QStringListModel>
#include <QFile>
#include <QMessageBox>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QDateTime>
#include "ui_mainwindow.h"

enum columnField{
    COL_FILENAME,
    COL_FILESIZE,
    COL_FILETYPE,
    COL_FILEDATE
};

namespace Ui {
    class MainWindow;
}

class MyFileSystemProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

    public:
        MyFileSystemProxyModel(QObject *parent = nullptr);
        QModelIndex             setRootPath(const QString &path);
        void                    setFilter(QDir::Filters filters);
        void                    setNameFilters(const QStringList &filters);
        QFileInfo               fileInfo(const QModelIndex &index) const;

    protected:
        bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

    private:
        QFileSystemModel*       myFileSystemModel;
};


class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow() override;

    private:
        Ui::MainWindow *ui;
        MyFileSystemProxyModel myModel;
};

#endif // MAINWINDOW_H
