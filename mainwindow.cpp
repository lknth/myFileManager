#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QModelIndex myIndex = myModel.setRootPath("C:/Qt");
    myModel.setFilter(QDir::NoDotAndDotDot | QDir::Files);
    ui->treeView->setModel(&myModel);
    ui->treeView->setRootIndex(myIndex);

    ui->treeView->setAlternatingRowColors(true);
    ui->treeView->header()->setDefaultAlignment(Qt::AlignBottom | Qt::AlignRight);

    // Setup ordering
    ui->treeView->sortByColumn(COL_FILENAME, Qt::AscendingOrder);

    // Set column width
    ui->treeView->header()->setMinimumSectionSize(QHeaderView::ResizeToContents);

    // Set the row height
    int height = fontInfo().pixelSize() * 3;
    QString myTreeViewStyleSheet = QString("QTreeView::item {min-height: %1 px;}").arg(height);
    ui->treeView->setStyleSheet(myTreeViewStyleSheet);
    ui->treeView->header()->setMinimumHeight(height);
}

MainWindow::~MainWindow()
{
    delete ui;
}


MyFileSystemProxyModel::MyFileSystemProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{
    myFileSystemModel = new QFileSystemModel(this);
    this->setSourceModel(myFileSystemModel);
}

bool MyFileSystemProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    if(left.column() == COL_FILESIZE)
        return myFileSystemModel->fileInfo(left).size() < myFileSystemModel->fileInfo(right).size();
    if(left.column() == COL_FILEDATE)
        return myFileSystemModel->fileInfo(left).lastModified().toSecsSinceEpoch() < myFileSystemModel->fileInfo(right).lastModified().toSecsSinceEpoch();
    return QSortFilterProxyModel::lessThan(left, right);
}

QModelIndex MyFileSystemProxyModel::setRootPath(const QString &path)
{
    return this->mapFromSource(myFileSystemModel->setRootPath(path));
}

void MyFileSystemProxyModel::setFilter(QDir::Filters filters)
{
    myFileSystemModel->setFilter(filters);
}

void MyFileSystemProxyModel::setNameFilters(const QStringList &filters)
{
    myFileSystemModel->setNameFilters(filters);
}

QFileInfo MyFileSystemProxyModel::fileInfo(const QModelIndex &index) const
{
    return myFileSystemModel->fileInfo(this->mapToSource(index));
}

